package com.jia.service;

import com.jia.pojo.User;

public interface UserService {
    public User queryUserByName(String name);
}
