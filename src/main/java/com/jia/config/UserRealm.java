package com.jia.config;

import com.jia.pojo.User;
import com.jia.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

//自定义的User Realm  需要继承AuthorizingRealm   实现认证和授权
public class UserRealm extends AuthorizingRealm {

    @Autowired
    UserService userService;
//    授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行了授权方法>>>doGetAuthorizationInfo");
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermission("user:add");

//        拿到当前登录的这个对象
        Subject subject = SecurityUtils.getSubject();
        User currentUser = (User) subject.getPrincipal();  //拿到user对象

        info.addStringPermission(currentUser.getPerms());
        return info;
    }
//      认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        System.out.println("执行了认证方法>>>doGetAuthenticationInfo");
//        获取当前的用户
//        Subject subject = SecurityUtils.getSubject();


        UsernamePasswordToken userToken = (UsernamePasswordToken)token;
//      连接真实数据库
        User user = userService.queryUserByName(userToken.getUsername());

        if (user == null){
            return null;  //抛出null的话就是用户名不存在
        }

//        当用户登录成功后将用户信息存入session中
        Subject currentSubject = SecurityUtils.getSubject();
        Session session = currentSubject.getSession();
        session.setAttribute("loginUser",user);
        return new SimpleAuthenticationInfo(user,user.getPwd(),"");
    }
}
